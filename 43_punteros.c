/*43_punteros.c
 *
 * Eleva al cubo una variable mediante una llamada por valor
 *
 *@author Franklin Lugo
 *@version 1.0
 *
*/

#include <stdio.h>

int cuboPorValor(int n); //prototipo

int main(){

	int numero = 5;
	printf("El valor original de numero es %d\n", numero );

	numero = cuboPorValor(numero);

	printf("El nuevo valor de numero es %d\n", numero );

	return 0;
}//fin main

//calcula y devuelve el cubo de un argumento entero
int cuboPorValor(int n){

	return n * n * n;

}//fin funcion