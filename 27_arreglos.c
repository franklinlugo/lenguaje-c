/*27_arreglos.c
 * 
 * Arreglo que cuenta la frecuencia de respuestas
 *    
 * A cuarenta estudiantes se les preguntó respecto a la calidad de la comida de la cafetería escolar, en una
 * escala de 1 a 10 (1 significa muy mala y 10 significa excelente). Coloque las 40 respuestas en un arreglo
 * entero que resuma los resultados de la encuesta.
 * @author Franklin Lugo
 * Version 1.0
*/ 
 
#include <stdio.h>
#define TR 40   //tamano respuesta
#define TF 11   //tamano frecuencia

int main () {
    
    int respuesta;  //contador de las 40 respuestas
    int rango;      //contador de los rangos del 1 al 10
    
    //iniciando los contadores de frecuencia
    int frecuencia[TF] = {0};
    
    //coloca las respuestas del examen dentro del arreglo respuestas
    int respuestas[TR] = {1, 2, 6, 4, 8, 5, 9, 7, 8, 10, 1, 6, 8, 16, 10, 3, 8, 2, 7, 6, 5, 7, 6, 8, 6, 5, 6, 6, 5, 6, 7, 5, 6, 4, 8, 6, 8, 10};
    
    /* por cada respuesta, seleciona el valor de un elemento del arreglo
    respuestas y utiliza dicho valor como subíndice en el arreglo
    frecuencia para determinar el elemento a incrementar */
    for(respuesta = 0; respuesta < TF; respuesta++) {
        ++frecuencia[respuestas[respuesta]];
    }//fin for
    
    //despliega los resultados
    printf("%s%17s\n", "Rango", "Frecuencia");
    
    //muestra las frecuencias en forma tabular
    for(rango = 1 ; rango < TF ; rango++ ) {
        printf("%3d%15d\n", rango, frecuencia[rango]);
    }//fin for
    return 0;
}//fin main
