/*72_strcat-strncat.c

uso de strcat y strncat

*/

#include <stdio.h>
#include <string.h>

int main()
{
	/*arreglos de caracteres*/
	char s1[20] = "feliz ";
	char s2[] = "anio nuevo ";
	char s3[40] = "";

	printf(" s1 = %s\ns2 = %s\n", s1, s2 );

	/*concatena s1 y s2*/
	printf( "strcat( s1, s2 ) = %s\n",strcat( s1, s2 ) );

	/*concatena los primeros 6 caracteres de s1 a s3. coloque '\0' despues del
	ultimo caracter */
	printf( "strncat( s3, s1, 6 ) = %s\n", strncat(s3, s1, 6) );

	/*concatena s1 a s3*/
	printf("strcat(s3, s1) = %s\n", strcat(s3, s1) );

	return 0;
}//fin main