/*56_punteros_a_funciones.c
 * 
 * (Un apuntador a una función es la dirección en donde reside el código de la función)
 * 
 * el siguiente programa consta de la función main y de
 * las funciones burbuja, intercambia, ascendente y descendente. La función ordenaMBurbuja
 * recibe como argumento un apuntador a una función, ya sea la función ascendente o la función descen-
 * dente, además del arreglo entero y el tamaño de éste. El programa indica al usuario que elija si el arreglo debe
 * ordenarse de manera ascendente o descendente. Si el usuario escribe 1, se pasa el apuntador a la función
 * 
 * @author Franklin Lugo
 * @version 1.0
*/

#include <stdio.h>
#define TAMANIO 10

/*Prototipos*/
void burbuja( int trabajo[], const int tamanio, int (*compara)( int a, int b ) );
int ascendente( int a, int b );
int descendente( int a, int b );

int main(){
	int orden; /*1  para el orden ascendente o 2 para el orden descendente*/
	int contador;

	int a[ TAMANIO ] = {2, 6, 4, 8, 10, 12, 89, 68, 45, 37};

	printf("Introduzca 1 para ordenar en forma ascendente, \n"
		"Introduzca 2 para ordenar en forma descendente: ");
	scanf("%d", &orden);

	printf("\nElementos de datos en el orden original\n");
	for (contador = 0; contador < TAMANIO; contador++)
	{
		printf( "%5d", a[contador] );
	}

	printf(" \n ");

	/*clasifica el arreglo en orden ascendente; 
	pasa la funcion ascendente como un argumento para especificar el orden ascendente*/
	if (orden == 1)
	{
		burbuja(a, TAMANIO, ascendente);
		printf("Elementos de datos en orden ascendente\n");

	}//fin if 
	else 
	{
		burbuja(a, TAMANIO, descendente);
		printf("Elementos de datos en orden descendente\n");
	}

	/*muestra el arreglo ordenado*/
	for (contador = 0; contador < TAMANIO; contador++)
	{
		printf( "%5d", a[contador] );
	}

	printf("\n");
	
	return 0;

}//fin main

/*ordenamiento burbuja multiproposito; el parametro compara es un apuntador a la funcion de comparacion que determina 
el tipo de ordenamiento*/
void burbuja( int trabajo[], const int tamanio, int (*compara)( int a, int b ) )
{
	int pasada;
	int cuenta;

	void intercambia(int *ptrElemento1, int *ptrElemento2);

	/*ciclo para controlar las pasadas*/
	for (pasada = 0; pasada < tamanio; pasada++)
	{
		/*ciclo para controlar el numero de comparaciones por pasada*/
		for (cuenta = 0; cuenta < tamanio - 1 ; cuenta++)
		{
			/* si los elementos adyancentes no se encuentran en orden los intercambia */
			if((*compara)(trabajo[cuenta], trabajo[cuenta + 1]))
			{
				intercambia( &trabajo[cuenta], &trabajo[cuenta + 1]);
			}//fin if
		}//fin for
	}//fin for
}//fin burbuja

/* intercambia los valores en las ubicaciones de memoria a las que apunta ptrElemento1 y prtElemento2 */
void intercambia( int *ptrElemento1, int *ptrElemento2)
{
	int almacena;

	almacena = *ptrElemento1;
	*ptrElemento1 = *ptrElemento2;
	*ptrElemento2 = almacena;
}//fin void intercambia

/* determina si los elementos estan en desorden para un ordenamiento ascendente */
int ascendente( int a, int b )
{
	return b < a; /* intercambia si b es menor que a */
}//fin ascendente

/* determina si los elementos estan en desorden para un ordenamiento descendente*/
int descendente( int a, int b )
{
	return b > a; /* intercambia si b es mayor que a*/
}//fin descendente