/* 55_barajar_cartas.c
 * 
 * Programa para barajar y repartir cartas
 *
 *@author Franklin Lugo
 *@version 1.0
 */


#include <stdio.h>
#include <stdlib.h>
#include <time.h>


/*Prototipos*/
void baraja( int wMazo[][13]);
void reparte( const int wMazo[][13], const char *wCara[], const char *wPalo[]);

int main(){
	/*inicializa el arreglo palos*/
	const char *palo[4] = {"Corazones", "Diamantes", "Treboles", "Espadas"};

	/*inicializa el arreglo cara*/
	const char *cara[13] = {"As", "Dos", "Tres",
	 "Cuatro", "Cinco", "seis", "Siete", "Ocho", "Nueve", "Diez", "Joto", "Quina", "Rey"};

	/*inicializa el arreglo mazo*/
	int mazo[4][13] = { 0 };

	srand( time(0)); /*semilla del generador de numeros aleatorios*/

	baraja(mazo);
	reparte(mazo, cara, palo);

	return 0;
}//fin main

/*funcion que baraja las cartas del mazo*/
void baraja( int wMazo[][13]){

	int fila;
	int columna;
	int carta;	/*contador*/

	/*elige aleatoriamente un espacio para cada una  de las 52 cartas*/
	for ( carta = 1; carta <= 52; carta++)
	{
		/*Elige una nueva ubicacion al azar hasta que encuentre un espacio vacio*/
		do
			{
				fila = rand() % 4;
				columna = rand() % 13;
			}while(wMazo[fila][columna] =! 0);

		/*coloca el numero de carta en el espacio vacio del mazo*/
		wMazo[fila][columna] = carta;
	}

}//fin baraja

/*reparte las cartas del mazo*/
void reparte( const int wMazo[][13], const char *wCara[], const char *wPalo[]){
	int carta;	/*contador*/
	int fila;
	int columna;

	/*reparte cada una de las 52 cartas*/

	for ( carta = 1; carta <= 52; carta++)
	{
		/*realiza el ciclo a traves de las filas del mazo*/
		for ( fila = 0; fila <= 3; fila++ )
		{
			/*realiza el ciclo a traves de las columnas de wMazo en la fila actual*/
			for ( columna = 0; columna <= 12; ++columna)
			{
				
				if( wMazo[fila][columna] == carta)
				{
					printf("%6s de %-9s%c", wCara[columna], wPalo[fila], carta % 2 == 0 ? '\n' : '\t' );
				}

			}
		
		}
				
	}
}//fin reparte
