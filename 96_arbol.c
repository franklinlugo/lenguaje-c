/*96_arbol.c

	crear un arbol binario y lo recorre en preorden, inorden y postorden

*/

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

/*Estructura autoreferenciada*/
struct nodoArbol
{
	struct nodoArbol *ptrIzq;	/* Apuntador al subarbol izquierdo */
	int dato;					/* Valor del nodo */
	struct nodoArbol *ptrDer;	/* Apuntador al subarbol derecho*/
};

typedef struct nodoArbol NodoArbol; /* Sinonimo de la estructura NodoArbol */
typedef NodoArbol *ptrNodoArbol;	/* Sinonimo de NodoArbol */

/* prototipos */
void insertaNodo(ptrNodoArbol *ptrArbol, int valor);
void inOrden(ptrNodoArbol ptrArbol);
void preOrden(ptrNodoArbol ptrArbol);
void postOrden(ptrNodoArbol ptrArbol);

int main()
{
	int i; 							/* contador para el ciclo de 1 a 10 */
	int elemento;					/* variable para almacenar valores al azar */
	ptrNodoArbol ptrRaiz = NULL;	/* árbol inicialmente vacío */

	srand( time( NULL ) );
	printf( "Los numeros colocados en el arbol son:\n" );

	/* inserta valores al azar entre 1 y 15 en el árbol */
	for ( i = 1; i <= 10; i++ ) {
		elemento = rand() % 15;
		printf( "%3d", elemento );
		insertaNodo( &ptrRaiz, elemento );
	} /* fin de for */

	/* recorre el árbol en preorden */
	printf( "\n\nEl recorrido en preorden es:\n" );
	preOrden( ptrRaiz );

	/* recorre el árbol en in inorden */
	printf( "\n\nEl recorrido inorden es:\n" );
	inOrden( ptrRaiz );

	/* recorre el árbol en posorden */
	printf( "\n\nEl recorrido en posorden es:\n" );
	postOrden( ptrRaiz );

	return 0; /* indica terminación exitosa */


}//fin main

/*Inserta un nodo dentro del arbol */
void insertaNodo(ptrNodoArbol *ptrArbol, int valor)
{

	/* si el árbol está vacío */
	if ( *ptrArbol == NULL ) {
	*ptrArbol = malloc( sizeof( NodoArbol ) );

	/* si el árbol está vacío */
	if ( *ptrArbol == NULL )
	{
		*ptrArbol = malloc( sizeof( NodoArbol ) );
		/* si la memoria está asignada, entonces asigna el dato */
		if ( *ptrArbol != NULL ) 
		{
			( *ptrArbol )->dato = valor;
			( *ptrArbol )->ptrIzq = NULL;
			( *ptrArbol )->ptrDer = NULL;
		} /* fin de if */
		else 
		{
			printf( "no se inserto %d. No hay memoria disponible.\n", valor );
		} /* fin de else */
	} /* fin de if */

	/* el árbol no está vacío */
	else 
	{ 
		/* el dato a insertar es menor que el dato en el nodo actual */
		if ( valor < ( *ptrArbol )->dato ) 
		{
			insertaNodo( &( ( *ptrArbol )->ptrIzq ), valor );
		} /* fin de if */

		/* el dato a insertar es mayor que el dato en el nodo actual */
		else if ( valor > ( *ptrArbol )->dato ) 
		{
			insertaNodo( &( ( *ptrArbol )->ptrDer ), valor );
		} /* fin de else if */

		else
		{ /* ignora el valor duplicado del dato */
			printf( "dup" );
		} /* fin de else */
	} /* fin de else */


}//fin insertarNodo


/* comienza el recorrido inOrden del arbol */
void inOrden(ptrNodoArbol ptrArbol)
{
	/* si el árbol no está vacío, entonces recórrelo */
	if ( ptrArbol != NULL )
	{
		inOrden( ptrArbol->ptrIzq );
		printf( "%3d", ptrArbol->dato );
		inOrden( ptrArbol->ptrDer );
	} /* fin de if */

}//fin inOrden

/* comienza el recorrido preorden del árbol */
void preOrden(ptrNodoArbol ptrArbol)
{
	/* si el árbol no está vacío, entonces recórrelo */
	if ( ptrArbol != NULL )
	{
		printf( "%3d", ptrArbol->dato );
		preOrden( ptrArbol->ptrIzq );
		preOrden( ptrArbol->ptrDer );
	} /* fin de if */

}//fin preOrden


/* Comiendo el recorrido postOrden del arbol */
void postOrden(ptrNodoArbol ptrArbol)
{
	/* si el árbol no está vacío, entonces recórrelo */
	if ( ptrArbol != NULL )
	{
		postOrden( ptrArbol->ptrIzq );
		postOrden( ptrArbol->ptrDer );
		printf( "%3d", ptrArbol->dato );
	}/* fin de if */
}//fin postOrden