/*36_burbuja.c
 *
 * Metodo de la burbuja
 * 
 * @author Franklin Lugo
 * @version 1.0 
*/ 

#include <stdio.h>
#define TAMANIO 10

int main(){
    
    int a[TAMANIO] = {2, 6, 4, 8, 10, 12, 89, 68, 45, 37};    //inicializa a
    int pasadas;                                                //contador pasadas
    int i;                                                      //contador comparaciones
    int almacena;                                               //ubicacion temporal utilizada para el intercambio de elementos
    
    printf("Elementos del arreglo en orden original\n");
    
    //mostrando el arreglo original
    for( i = 0 ; i < TAMANIO ; i++ ){
        printf( "%4d", a[i]);
    }//fin for

    printf("\n");

    /*
     * ordenamiento burbuja
     * ciclo para controlar el numero de pasos*/
    for( pasadas = 0 ; pasadas < TAMANIO ; pasadas++ ){
    
        //ciclo que controla el numero de comparaciones por pasada
        for( i = 0 ; i < TAMANIO - 1; i++){
            
            //compara los elementos adyancentes y los intercambia si el 
            //primer elemento es mayor que el segundo 
            if(a[i] > a[i+1]){
                almacena    =  a[i];
                a[i]        = a[i+1];
                a[i+1]      = almacena;
            }//fin if
            
        }//fin for interno

    }//fin for externo

    printf("Elementos del arreglo en orden ascendente\n");
    for( i = 0 ; i < TAMANIO ; i++ ){
        printf( "%4d", a[i]);
    }//fin for
    
    printf("\n");

    return 0;
}//fin main
