/*78_printf.c

	uso de la precision durante la impresion de enteros, numeros de punto flotante y cadenas
	
*/

#include <stdio.h>

int main()
{
	int i = 873;
	double f = 123.94536;
	char s[] = "Feliz cumpleanios";

	printf( "Uso de la precision en enteros\n" );
	printf( "\t%.4d\n\t%.9d\n\n", i, i );

	printf( "Uso de la precision en numeros de punto flotante\n" );
	printf( "\t%.3f\n\t%.3e\n\t%.3g\n\n", f, f, f );

	printf( "Uso de la precision en cadenas\n" );
	printf( "\t%.18s\n", s );



	return 0;
}//fin main