/*21_factorial.c
 * 
 * calculo del factorial de un numero utilizando recursividad
 * 
 * @author Franklin Lugo
 * Version 1.0
*/ 

#include <stdio.h>

long factorial(long numero); //prototipo de la funcion

int main () {
    
    int i; // contador 
    
    /*Repite 11 veces, durante cada iteracion calcula el factorial (i) y 
     * y despliega el resultado*/
     
    for ( i = 0; i <= 10 ; i++ ) {
        printf("%2d! = %ld\n", i, factorial(i));
    }//fin for 

    return 0;
}//fin main

//Definicion recursiva de la funcion factorial
long factorial(long numero) {
    //caso base
    if (numero <= 1) {
        return 1;
    }//fin if
    else{ 
        return (numero * factorial (numero - 1));  // paso recursivo
    }//fin else

}//fin funcion factorial
