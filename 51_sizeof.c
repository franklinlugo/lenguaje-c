/*51_sizeof.c
 *
 * cuando el operador sizeof se utiliza en un nombre de un arreglo,
 * este devuelve el numero de bytes en el arreglo
 *
 *@author Franklin Lugo
 *@version 1.0
 */

#include <stdio.h>

size_t obtieneTamanio(float *ptr); /*prototipo*/



int main(){

	float arreglo[20];

	printf("El numero de bytes en el arreglo es %d"
		"\nEl numero de bytes devueltos por obtieneTamanio es %d\n",
		sizeof(arreglo), obtieneTamanio(arreglo));

	return 0;
}//fin main

/*devuelve el tamano de prt*/
size_t obtieneTamanio(float *ptr){
	return sizeof(ptr);
}//fin funcion
