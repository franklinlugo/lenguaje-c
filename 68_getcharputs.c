/*68_getcharputs.c

uso de getchar y puts

@author Franklin Lugo
@version 1.0
*/

#include <stdio.h>

int main()
{
	char c;		/*variable para almacenar los caracteres*/
	char enunciado[80]; 	/*crea un arreglo de caracteres*/
	int i = 0;		/*contador*/

	puts("introduzca una linea de texto:\n");

	/*utiliza getchar para leer cada caracter*/
	while( ( c = getchar() ) != '\n') {
			enunciado[i++] = c;
		}

	enunciado[i] = '\0'; /*termina la cadena*/

	/*utiliza puts para desplegar el enunciado*/
	puts("\nLa linea introducida es: ");
	puts(enunciado);

	return 0;
}//fin main