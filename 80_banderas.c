/*80_banderas.c

	impresion de numeros con o sin la bandera

*/

#include <stdio.h>

int main()
{
	printf( "%d\n%d\n", 786, -786 );
	printf( "%+d\n%+d\n", 786, -786 );

	return 0;
}//fin main