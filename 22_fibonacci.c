/*22_fibonacci.c
 * 
 * calculo del factorial de un numero utilizando recursividad
 * 
 * @author Franklin Lugo
 * Version 1.0
*/ 

#include <stdio.h>

long fibonacci(long n );    //prototipo de la funcion 

int main() {
    long resultado; // valor fibonacci
    long numero;    //numero a introducir por el usuario
    
    printf("Introduzca un entero: ");
    scanf("%ld", &numero);
    
    resultado = fibonacci(numero);
    
    printf("Fibonacci (%ld) = %ld\n", numero, resultado);
    
    
    return 0;
}//fin main

//definicion de la funcion recursiva fibonacci

long fibonacci(long n){
    ///caso base
    if (n == 0 || n == 1) {
        return n;
    }//fin if
    else{
        return fibonacci(n - 1) + fibonacci(n  -2);
    }//fin else
    

}//fin fibonacci
