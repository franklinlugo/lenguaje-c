/*
 * uso de la instruccion de repeticion do while  - 11_dowhile.c
 * 
 * @author Franklin Lugo
 * @version 1.0
*/ 

#include <stdio.h>

int main () {

    int contador = 1;
    
    do{
        
        printf("%d ", contador);
        
    } while( ++contador <= 10);
    
    return 0;
}
