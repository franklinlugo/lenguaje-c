/*
 * uso de continue dentro de un for  - 11_dowhile.c
 * 
 * @author Franklin Lugo
 * @version 1.0
*/ 
#include <stdio.h>

int main () {

    int x; // contador 
    
    for (x = 1; x <= 10; x++) {
        
        if (x == 5) continue; // ignora el resto del codigo en el cuerpo de ciclo
        
        printf("%d ", x); // imprime valor de x
    
    }// fin for
    
    printf("\nUtiliza continue para ignorar la impresion del valor 5\n");
    
    return 0;

}
