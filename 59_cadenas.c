/*59_cadenas.c

Uso de las funciones islower, isupper, tolower, touppe

el siguiente programa muestra las funciones islower, isupper, tolower y toupper. La función islower
determina si su argumento es una letra minúscula (a-z). La función isupper determina si su argumento es
una letra mayúscula (A-Z). La función tolower convierte una letra mayúscula a minúscula y devuelve la 
letra minúscula. Si el argumento no es una letra mayúscula, tolower devuelve el argumento sin cambio. 
La función toupper convierte una letra minúscula a una letra mayúscula y devuelve la letra mayúscula. 
Si el argumento no es una letra minúscula, toupper devuelve el argumento sin cambio.


@author Franklin Lugo
@version 1.0
*/

#include <stdio.h>
#include <ctype.h>

int main()
{
	printf( "%s\n%s%s\n%s%s\n%s%s\n%s%s\n\n",
		"De acuerdo con islower:",
		islower( 'p' ) ? "p es una " : "p no es una ", 
		"letra minuscula",
		islower( 'P' ) ? "P es una " : "P no es una ",
		"letra minuscula",
		islower( '5' ) ? "5 es una " : "5 no es una ",
		"letra minuscula",
		islower( '!' ) ? "! es una " : "! no es una ",
		"letra minuscula" );


	printf( "%s\n%s%s\n%s%s\n%s%s\n%s%s\n\n",
		"De acuerdo con isupper:",
		isupper( 'D' ) ? "D es una " : "D no es una ",
		"letra mayuscula",
		isupper( 'd' ) ? "d es una " : "d no es una ",
		"letra mayuscula",
		isupper( '8' ) ? "8 es una " : "8 no es una ",
		"letra mayuscula",
		isupper( '$' ) ? "$ es una " : "$ no es una ",
		"letra mayuscula" );

	printf( "%s%c\n%s%c\n%s%c\n%s%c\n",
		"u convertida a mayuscula es ", toupper('u'),
		"7 convertida a mayuscula es ", toupper('7'),
		"$ convertida a mayuscula es ", toupper('$'),
		"L convertida a mayuscula es ", toupper('L'));

return 0;

}//fin main
