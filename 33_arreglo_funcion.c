/*33_arreglo_funcion.c
 *
 * El nombre de un arreglo es lo mismo que &arreglo[ 0 ]
 *
 * @author Franklin Lugo
 * @version 1.0 
*/ 

#include <stdio.h>

int main(){

    char arreglo[5];    //define un arreglo de 5 elementos
    
    printf("arreglo = %p\n&arreglo[0] = %p\n" "&arreglo = %p\n", arreglo, &arreglo[0], &arreglo);
    
    return 0;
}
