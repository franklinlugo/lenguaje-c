/*84_scan.c

	uso de un conjunto de exploracion

*/

#include <stdio.h>

int main()
{
	char z[9];

	printf( "Introduzca una cadena\n");
	scanf( " %[aeiou]", z); /*busca un conjunto de caracteres*/

	printf("La entrada es \"%s\" \n", z);
	return 0;
}//fin main