/*
 * Cuenta las calificaciones expresadas en letras - 10_swith.c
 * 
 * @author Franklin Lugo
 * @version 1.0
*/ 

#include <stdio.h>

int main () {
    
    int calificacion;
    int cuentaA = 0;
    int cuentaB = 0;
    int cuentaC = 0;
    int cuentaD = 0;
    int cuentaF = 0;

    printf("Introduzca la letra que correspone a la calificacion.\n");
    printf("Introduzca el caracte EOF para finalizar la entrada de datos\n");
    
    //repite hasta que se digita la secuencia de fin de archivo
    //En unix el caracter EOF es ctrl + d y en windows ctrl + z
    while ((calificacion = getchar() ) != EOF) {
        
        switch (calificacion) {
            
            case 'A':
            case 'a':
                ++cuentaA;
                break; // necesario para salir de switch
            
            case 'B':
            case 'b':
                ++cuentaB;
                break; 
        
            case 'C':
            case 'c':
                ++cuentaC;
                break;
                
            case 'D':
            case 'd':
                ++cuentaD;
                break;
                
            case 'F':
            case 'f':
                ++cuentaF;
                break;

            case '\n': // ignora nuevas lineas
            case '\t': // ignora tabuladores
            case ' ': // ignora espacios
                break;
            
            default: // atrapa a los demas caracteres
                printf("Introdujo una letra incorrecta.");
                printf(" Introduzca una nueva calificacion: ");
                break; //opcional, de igual manera saldra de switch
                

        }//fin switch
    }// fin while
    
    // resumen de los resultados
    printf("Los totales por calificacion son:\n");
    printf("A: %d\n", cuentaA);
    printf("B: %d\n", cuentaB);
    printf("C: %d\n", cuentaC);
    printf("D: %d\n", cuentaD);
    printf("F: %d\n", cuentaF);
    
    return 0; // indica terminacion exitosa del programa
}//fin main
