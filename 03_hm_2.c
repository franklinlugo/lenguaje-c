/*
 * Practica hola mundo en lenguaje c - hm_2.c
 * 
*/

/*
 * directiva del procesador. Indica al preprocesador que incluya en el 
 * programa el contenido del encabezado estandar  de entrada/salida
*/ 
#include <stdio.h>

/*
 * todo programa en c comienza su ejecucion en la funcion main
*/ 
int main (void) {
    
    // la diagonal invertida \ se conoce como caracter de escape
    printf("Bienvenidos al curso de lenguaje c\n");
    
    return 0;

} // cierre de la funcion main
