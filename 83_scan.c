/*83_scan.c

	lectura de caracteres y cadenas

*/

#include <stdio.h>

int main()
{
	char x;
	char y[9];

	printf("Introduzca una cadena: ");
	scanf( "%c%s", &x, y );

	printf("La entrada fue:\n");
	printf("El caracter \"%c\" ", x);
	printf("Y la cadena \"%s\" \n", y );
	
	return 0;
}//fin main
