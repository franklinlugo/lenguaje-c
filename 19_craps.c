/*19_craps.c
 * juego de dados craps
 * 
 * El jugador tira los dados y se calcula la suma de ambos.
 * Si la suma en igual a 7 u 11 en el primer tiro, el jugador gana
 * Si la suma es 2, 3 o 12 en el primer tiro (craps) el jugador pierde
 * Si la suma es 4, 5, 6, 8, 9 o 10 en el primer tiro, la suma se convierte en el punto del jugador
 * Para ganar debe continuar tirando los dados hasta hacer su punto
 * El jugador pierde si tira un 7 antes de hacer el punto
 * 
*/ 

#include <stdio.h>
#include <stdlib.h>
#include <time.h> // contiene prototipo de la funcion time

/* Crea un tipo definido por el programador llamado enumeracion
 * Una enumeración, definida mediante la palabra reservada enum, es un
 * conjunto de constantes enteras representadas por medio de 
 * identificadores.
*/

enum Estatus { CONTINUA, GANA, PIERDE };

int tiraDados (void); // prototipo de la funcion

int main () { 
    
    // constantes de enumeracion que representan el status del juego
    
    int suma;       // suma del tiro de datos
    int miPunto;    // mi punto ganado
    
    /* puede contener CONTINUA, GANA O PIERDE
     * Definida para que sea del nuevo tipo enum Estatus    
     * */
    enum Estatus estatusJuego; 
    
    
    srand( time ( NULL ) ); // randomiza el generador de numeros aleatorios
	
    suma = tiraDados(); //Primer tiro de los dados
    
    
    // para determinar el estado del juego segun la suma
    switch(suma) { 
        
        //gana en el primer tiro
        case 7:
        case 11:
            estatusJuego = GANA;
            break;
        
        //pierde en el primer tiro
        case 2:
        case 3:
        case 12:
            estatusJuego = PIERDE;
            break;
            
        //recuerda el punto
        default:
            estatusJuego = CONTINUA;
            miPunto      = suma;
            printf("Su punto es %d\n", miPunto);
            break; // opcional
    
    } // fin switch
    
    // while para controlar mientras el juego no se complete
    while ( estatusJuego == CONTINUA ) {
        
        suma = tiraDados(); // tira de nuevo los dados 
        
        if( suma == miPunto) {
            estatusJuego = GANA; // fin de juego jugador gana 
        } // fin if
        else {
            if (suma == 7 ) estatusJuego = PIERDE;
        } // fin else
    
    } // fin while
    
    // despliega mensaje de triunfo o derrota
    
    if ( estatusJuego == GANA) printf("El jugador gana");
    else printf("El jugador pierde");
    
    return	0; 

} // fin main 

// calcula la suma y despliega los resultados
int tiraDados (void) {

    int dado1;      // primer dado
    int dado2;      // segundo dado
    int sumaTemp;   // suma de los dados
    
    dado1       = 1 + (rand() % 6); //aleatorio para el dado 1
    dado2       = 1 + (rand() % 6); //aleatorio para el dado 2
    sumaTemp    = dado1 + dado2;
    
    //despliega los resultados de este tiro
    printf("El jugador tiro %d + %d = %d\n", dado1, dado2, sumaTemp);
    
    return sumaTemp;
    
} // fin tiraDados