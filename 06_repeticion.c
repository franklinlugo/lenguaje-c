/*
 * Programa para obtener promedio de calificaciones de un grupo
 * mediante repeticion controlada por centinela - 06_repeticion.c
 * 
 * @author Franklin Lugo
 * @version 1.0
*/ 

#include <stdio.h>

int main () {

    int contador;       // numero de calificacion siguiente
    int calificacion;   // valor de la calificacion
    int total;          // suma de las calificaciones
    
    float promedio;       // variable con punto decimal para el promedio
    
    // inicializacion de variables
    total = 0;
    contador = 0;
    
    printf("Introduzca la calificacion, -1 para terminar: ");
    scanf("%d", &calificacion);
    
    /*
     * ciclo que se ejecutara mientra calificacion sea diferente de -1
     * el cual es el valor centinela
    */ 
    while (calificacion != -1) {
    
        total       = total + calificacion;
        contador    = contador + 1;
        
        // para obtener la siguienta calificacion
        
        printf("Introduzca la calificacion, -1 para terminar ");
        scanf("%d", &calificacion);
    } // fin del while
    
    /*
     * para determinar si el usuario introdujo por lo menos un datos
    */ 
    if (contador != 0) {
        
        /*
         * al dividir dos enteros  se obtiene como resultado una division
         * entera, en la cual se pierde la parte fraccional, es decir
         * se trunca
        */ 
        promedio = (float) total / contador; // evita que se trunque 
        
        printf("El promedio del grupo es: %.2f", promedio);

    }
    else {
        
        printf("No se introdujeron calificaciones");

    }
    
    return 0;

} // fin de la funcion main
