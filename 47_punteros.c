/*47_punteros.c
 *
 * Intenta modificar un dato a traves de un apuntador 
 * no constante a un dato constante
 *
 *@author Franklin Lugo
 *@version 1.0
 *
*/

#include <stdio.h>

void f(const int *prtX); /*prototipo*/

int main(){

	int y;

	f(&y);

	return 0;

}//fin main

/*no se puede utilizar  ptrX para modificar el valor de la variable
a la cual apunta*/
void f(const int *prtX){
	*ptrX = 100; /*error*/
}//fin funcion
