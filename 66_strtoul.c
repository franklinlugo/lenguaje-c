/*66_strtoul.c

uso de strtould

La función strtoul convierte a unsigned long una secuencia de caracteres que
representa un entero de tipo unsigned long. La función trabaja de la misma
forma que la función strtol. La instrucción

	x = strtoul( cadena, &ptrResiduo, 0 );

indica que a x se le asigna el valor unsigned long convertido de la cadena. Al
segundo argumento, &ptrResiduo, se le asigna el resto de cadena después de
la conversión. El tercer argumento, 0, indica que el valor a convertirse
puede estar en formato octal o hexadecimal.

@author Franklin Lugo
@version 1.0
*/

#include <stdio.h>
#include <stdlib.h>

int main()
{
	const char *cadena = "1234567abc"; /*inicializa el apuntador cadena*/
	unsigned long x;	/*Variable  to hold converted sequence*/
	char *ptrResto;		/*crea un apuntador a char*/

	x = strtoul(cadena, &ptrResto, 0);

	printf( "%s\"%s\"\n%s%lu\n%s\"%s\"\n%s%lu\n",
		"La cadena original es ", cadena,
		"El valor convertido es ", x,
		"El resto de la cadena original es ", ptrResto,
		"El valor convertido menos 567 es ", x - 567 );


	return 0;
}//fin main