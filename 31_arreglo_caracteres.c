/*31_arreglo_caracteres.c
 * 
 * inicializacion de un arreglo de caracteres mediante un literal de
 * cadena, la lectura de una cadena que se encuentra que se encuentra en
 * un arreglo de caracteres, la impresion de un arreglo de carateres como
 * cadena, y el acceso a los caracteres individuales  de la cadena
 *
 * @author Franklin Lugo
 * @version 1.0 
*/ 

#include <stdio.h>

int main() {

    char cadena1[20];                       //Reserva 20 caracteres
    char cadena2[] = "Literal de cadena";   //Reserva 18 caracteres
    int i;          //contador
    
    //lee una cadena del usuario y la introduce  en el arreglo cadena1
    printf("Introduce una cadena: ");
    scanf("%s", cadena1);   //entrada que finaliza con una espacio en blanco  
    
    //muestra las cadenas
    printf("La cadena1 es: %s\ncadena2 es: %s\n"
    "la cadena1 con espacios entre caracteres es:\n", cadena1, cadena2);
    
    //muestra los caracteres hasta que encuentra el caracter nulo
    for( i = 0 ; cadena1[i] != '\0' ; i++ ){
        printf("%c", cadena1[i]);
        
    printf("\n");
    }//fin for
    return 0;
}//fin main 
