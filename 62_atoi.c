/*62_atof.c

Uso de atoi

La función atoi convierte su argumento, una cadena de dígitos que representa un
entero, a un valor int. La función devuelve el valor int. Si el valor convertido
no puede representarse, el comportamiento de la función atoi es indefinido.


@author Franklin Lugo
@version 1.0
*/

#include <stdio.h>
#include <stdlib.h>

int main()
{
	int i;
	i = atoi("2593");

	printf("%s%d\n%s%d\n",
	"La cadena \"2593\" Convertida a int es ", i,
	"El valor convertido menos 593 es ", i - 593 );
	return 0;
}//fin main