/* 53_arreglo_apuntadores.c
 *
 * Uso de las notaciones de subindices  y de apuntadores con arreglos
 *
 *
 *@author Franklin Lugo
 *@version 1.0
 */

#include <stdio.h>

int main(){
	
	int b[] = {10, 20, 30, 40};
	int *ptrB = b;

	int i;
	int desplazamiento;

	/*muestra el arreglo b con la notacion de subindices*/
	printf("El arreglo b impreso con:\nNotacion de subindices de arreglos\n");

	for( i = 0 ; i < 4 ; i++ ){
		printf("b[%d] = %d\n", i, b[i] );
	}//

	/*muestra el arreglo b mediante el uso del nombre del arreglo y notacion
	apuntador/desplazamiento*/
	printf("notacion apuntador/desplazamiento donde\n"
		"el apuntador es el nombre del arreglo\n");

	for( desplazamiento = 0; desplazamiento < 4 ; desplazamiento++){
		printf("*(b + %d) = %d\n", desplazamiento, *(b + desplazamiento) );
	}//

	/*muestra el arreglo b mediane el uso  de ptrB y notacion de subindices de arreglos */
	printf("\nNotacion de subindices de arreglos\n");

	for( i = 0 ; i < 4 ; i++ ){
		printf("ptrB[%d] = %d\n", i, ptrB[i] );
	}//

	/*muestra el arreglo b mediante el uso de ptrB y notacion de apuntador/desplazamiento*/
	printf("\nnotacion apuntador / desplazamiento\n");

	for( desplazamiento = 0 ; desplazamiento < 4 ; desplazamiento++ ){
		printf("*(prtB + %d) = %d\n", desplazamiento, *(ptrB + desplazamiento) );
	};
	return 0;

}//fin main