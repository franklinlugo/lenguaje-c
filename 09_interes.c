/*
 * calculo de interes compuesto anual - 09_interes.c
 * 
 * @author Franklin Lugo
 * @version 1.0
*/

#include <stdio.h> 
#include <math.h> 


int main () {
        
    double monto;                      // monto del deposito
    double principal    = 1000.0;     // monto principal 
    double tasa         = .05;          // interes compuesto anual
    int anio;                           // contador de anios
    
    // print que muestra el encabezado de salida de la tabla
    printf("%4s%21s\n", "anio", "Monto del deposito");
    
    // for que calcula el monto del deposito para c/u de los anios
    for (anio = 1; anio <= 10; anio++) {
        
        // calcula el nuevo monto para el anio especificado
        monto = principal * pow (1.0 + tasa, anio);
    
        printf("%4d%21.2f\n", anio, monto);
    }// fin for
    return 0;
}//fin main
