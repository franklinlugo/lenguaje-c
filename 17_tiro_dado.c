/*17_tiro_dado.c
 * tiro de un dado de 6 lados 6000  veces
 * 
*/ 

#include <stdio.h>
#include <stdlib.h>

int main () {
	
	int tiro;
	int cara;

	int fq1 = 0; 
	int fq2 = 0; 
	int fq3 = 0; 
	int fq4 = 0; 
	int fq5 = 0; 
	int fq6 = 0; 
	
	for (tiro = 0 ; tiro <= 6000; tiro++) {
		
		cara = 1 + rand() % 6 ; // numeros aleatorios entre 1 y 6
		
		switch (cara) {
			case 1:
				fq1++;
				break;
				
			case 2:
				fq2++;
				break;
				
			case 3:
				fq3++;
				break;
				
			case 4:
				fq4++;
				break;
				
			case 5:
				fq5++;
				break;
				
			case 6:
				fq6++;
				break;
				
		}// fin switch
		
	} // fin for

	/* Despliega los resultados en forma tabular*/
	
	printf("%s%13s\n", "Cara", "Frecuencia");
	printf("1%13d\n", fq1);
	printf("2%13d\n", fq2);
	printf("3%13d\n", fq3);
	printf("4%13d\n", fq4);
	printf("5%13d\n", fq5);
	printf("6%13d\n", fq6);

	return	0; 

} // fin main 
