/*65_strtol.c

uso de strtol

La función strtol convierte a long una secuencia de caracteres que representa un entero.
La función recibe tres argumentos, una cadena (char *), un apuntador a una cadena y un
entero. La cadena contiene la secuencia de caracteres a convertir. El apuntador se asigna
a la ubicación del primer carácter después de la parte convertida de la cadena. El entero
especifica la base del valor que se convierte. La instrucción

x = strtol( cadena, &ptrResiduo, 0 );

indica que a x se le asigna el valor long convertido de la cadena. Al segundo argumento,
ptrResiduo, se le asigna el residuo de la cadena después de la conversión. El uso de NULL
para el segundo argumento provoca que se ignore el resto de la cadena. El tercer argumento,
0, indica que el valor a convertir puede estar en formato octal (base 8), decimal (base 10)
o hexadecimal (base 16). La base se puede especificar como 0 o cualquier valor
entre 2 y 36. 
		
@author Franklin Lugo
@version 1.0

*/


#include <stdio.h>
#include <stdlib.h>

int main(){

	const char *cadena = "-1234567abc";
	char *ptrResto;
	long x;

	x = strtol(cadena, &ptrResto, 0);

	printf( "%s\"%s\"\n%s%ld\n%s\"%s\"\n%s%ld\n",
		"La cadena original es ", cadena,
		"El valor convertido es ", x,
		"El resto de la cadena original es ",
		ptrResto,
		"El valor convertido mas 567 es ", x + 567 );

	return 0;
}//fin main
