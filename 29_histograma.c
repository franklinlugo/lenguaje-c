/*29_histograma.c
 *
 * Graficar los elementos de un arreglo mediante un histograma
 * 
 * 
 * @author Franklin Lugo
 * @version 1.0 
*/ 
 

#include <stdio.h>
#define TAMANIO 10

int main() {

    int n[TAMANIO] = {19, 13, 15, 7, 11, 9, 13, 5, 17, 1};
    int i;      //contador for externo para los elementos del arreglo
    int j;      //contador for interno cuenta *s en cada barra del histograma
    
    printf("%s%13s%17s\n", "Elemento", "Valor", "Histograma");
    
    //para cada elemento del arreglo n muestra una barra del histograma
    for( i = 0 ; i < TAMANIO ; i++ ){
        
        printf("%7d%13d     ", i, n[ i ]);
        
        for( j = 0 ; j <= n[i] ; j++ ){
            printf("%c", '*');
        }//for interno 
        
    printf("\n");//fin de una barra del histograma
    
    }//for externo
    
    return 0;
}//fin main
