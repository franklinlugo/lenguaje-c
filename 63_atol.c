/*63_atol.c

Uso de atol

La función atol convierte su argumento, una cadena de dígitos que representa
un entero largo, a un valor long. La función devuelve el valor long. Si el valor
convertido no puede representarse, el comportamiento de la función atol es
indefinido. Si int y long se almacenan en cuatro bytes, las funciones atoi y atol
trabajan de manera idéntica.

@author Franklin Lugo
@version 1.0
*/

#include <stdio.h>
#include <stdlib.h>

int main()
{
	long l;
	l = atol("1000000");

	printf("%s%ld\n%s%ld\n",
		"la cadena \"1000000\" convertida a long int es ", l,
		"El valor convertido, dividido entre 2  es ", l / 2);

	return 0;
}//fin main