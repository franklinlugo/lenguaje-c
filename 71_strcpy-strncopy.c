/*71_strcopy-strncpy.c

uso de strcpy y strncpy

La biblioteca de manipulación de cadenas (<string.h>) proporciona muchas funciones 
útiles para manipular datos de cadena (copiar y concatenar cadenas), comparar cadenas,
buscar caracteres y otras cadenas dentro de cadenas, separar cadenas en tokens
(separar cadenas en su piezas lógicas) y determinar la longitud de cadenas.

EL siguiente programa utiliza strcpy para copiar la cadena completa del arreglo x dentro
del arreglo y, y utiliza strncpy para copiar los primeros 14 caracteres del arreglo 
x dentro del arreglo z. Se agrega un carácter nulo (‘\0’) al arreglo z, debido
a que la llamada a strncpy en el programa no escribe un carácter de terminación nulo
(el tercer argumento es menor que la longitud de la cadena del segundo argumento).

*/

#include <stdio.h>
#include <string.h>

int main()
{
	char x[] = "Hola companieros de clases";
	char y[25];
	char z[15];

	/*contenido de la copia de x en y*/
	printf("%s%s\n%s%s\n",
		"La cadena en el arreglo x es: ", x,
		"la cadena en el arreglo y es: ", strcpy(y, x));

	/*copia los primeros 17 caracteres de x dentro de z.
	no copia el caracter nulo*/
	strncpy( z, x, 17 );

	z[17] = '\0'; /*termina la cadena en z*/
	printf("La cadena en el arreglo z es: %s\n", z );

	return 0;
}//fin main