/*42_punteros.c
 *
 * Uso de los operadores & y *
 *
 *@author Franklin Lugo
 *@version 1.0
 *
*/

#include <stdio.h>

int main(){
	int a;		//entero
	int *ptrA;	//apuntador a un entero

	a 		= 7;
	ptrA	= &a;	//prtA toma la direccion de a

	printf("La direccion de a es %p""\nEl valor de prtA es %p", &a, ptrA );

	printf("\n\nEl valor de a es %d""\nEl valor de *prtA es %d\n", a, *ptrA );

	printf("\n\nMuestra de que * y & son complementos "
		"uno del otro\n&*ptrA = %p"
		"\n*&ptrA = %p\n", &*ptrA, *&ptrA);

	return 0;

}//fin main