/*
 * Uso de un ciclo for para sumar numeros - 08_for.c
 * 
 * @author Franklin Lugo
 * @version 1.0
*/ 

#include <stdio.h>

// la funcion main comienza la ejecucion del programa
int main () {
    
    int suma = 0;
    int numero;
    
    for (numero = 2; numero <= 100 ; numero += 2) {
        
        suma =+ numero; // suma el numero a la suma
        
    }
    
    printf("La suma es %d\n", suma);
    
    return 0;
}
