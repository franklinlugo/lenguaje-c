/*45_punteros.c
 *
 * conversion de letras minusculas a letras mayusculas
 * mediante un apuntador no constante a un dato no constante
 *
 *@author Franklin Lugo
 *@version 1.0
 *
*/

#include <stdio.h>
#include <ctype.h>

void convierteAMayusculas(char *prtS); /*prototipo*/
int main(){

	char cadena[] = "caracteres y $32.98";

	printf("La cadena antes de la conversion es: %s\n", cadena );

	convierteAMayusculas(cadena);

	printf("La cadena despues de la conversion es: %s\n", cadena );
	return 0;
}//fin main

void convierteAMayusculas(char *prtS){
	while(*prtS != '\0'){
		if( islower(*prtS)){
			*prtS = toupper(*prtS);
		}//fin if
		++prtS; //mueve el *prtS al siguiente caracter
	}//fin while 
}//fin funcion
