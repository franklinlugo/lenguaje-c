/*58_cadenas.c

Uso de las funciones isdigit, isalpha, isalnum, e isxdigit


el siguiente programa muestra las funciones isdigit, isalpha, isalnum e isxdigit. La función
isdigit determina si su argumento es un dígito (0-9). La función isalpha determina si su argumento es
una letra mayúscula (A-Z), o una letra minúscula (a-z). La función isalnum determina si su argumento es una
letra mayúscula, una letra minúscula o un dígito. La función isxdigit determina si su argumento es un dígito
hexadecimal (A-F, a-f, 0-9).

@author Franklin Lugo
@version 1.0
*/

#include <stdio.h>
#include <ctype.h>

int main()
{
	printf( "%s\n%s%s\n%s%s\n\n", "De acuerdo con isdigit: ",
		isdigit( '8' ) ? "8 es un " : "8 no es un ", "digito",
		isdigit( '#' ) ? "# es un " : "# no es un ", "digito" );

	printf( "%s\n%s%s\n%s%s\n%s%s\n%s%s\n\n",
		"De acuerdo con isalpha:",
		isalpha( 'A' ) ? "A es una " : "A no es una ", "Letra",
		isalpha( 'b' ) ? "b es una " : "b no es una ", "Letra",
		isalpha( '&' ) ? "& es una " : "& no es una ", "Letra",
		isalpha( '4' ) ? "4 es una " : "4 no es una ", "Letra" );

	printf( "%s\n%s%s\n%s%s\n%s%s\n\n",
		"De acuerdo con isalnum:",
		isalnum( 'A' ) ? "A es un " : "A no es un ",
		"digito o una letra",
		isalnum( '8' ) ? "8 es un " : "8 no es un ",
		"digito o una letra",
		isalnum( '#' ) ? "# es un " : "# no es un ",
		"digito o una letra" );

	printf( "%s\n%s%s\n%s%s\n%s%s\n%s%s\n%s%s\n",
		"De acuerdo con isxdigit:",
		isxdigit( 'F' ) ? "F es un " : "F no es un ", 
		"digito hexadecimal",
		isxdigit( 'J' ) ? "J es un " : "J no es un ",  
		"digito hexadecimal",
		isxdigit( '7' ) ? "7 es un " : "7 no es un ", 
		"digito hexadecimal",
		isxdigit( '$' ) ? "$ es un " : "$ no es un ", 
		"digito hexadecimal",
		isxdigit( 'f' ) ? "f es un " : "f no es un ", 
		"digito hexadecimal" );


return 0;	


}//fin main