/*57_punteros_a_funciones.c
 *
 * (Un apuntador a una función es la dirección en donde reside el código de la función)
 * 
 * Demostracion de un arreglo de apuntadores a funciones
 * 
 * Un uso común de los apuntadores a funciones se encuentra en los llamados sistemas basados en menús. A un usuario
 * se le indica que seleccione una opción desde un menú (posiblemente de 1 a 5). Cada opción se sirve de una
 * función diferente. Los apuntadores a cada función se almacenan en un arreglo de apuntadores a funciones. Las opciones
 * del usuario se utilizan como subíndices del arreglo, y el apuntador en el arreglo se utiliza para llamar a una
 * función.
 * 
 * ejemplo genérico de la mecánica para definir y utilizar un arreglo de apuntadores a funciones. Se definen tres funciones,
 * funcion1, funcion2 y funcion3, las cuales toman un argumento entero y no devuelven valor alguno. Los apuntadores
 * a estas tres funciones se almacenan en el arreglo f, el cual se define de la siguiente manera:
 * void ( *f[ 3 ] )( int ) = { funcion1, funcion2, funcion3 };
 * 
 * La definición se lee desde el paréntesis que se encuentra hasta la izquierda, “f es un arreglo de 3 apuntadores
 * a funciones que toman un int como argumento y que devuelven void”. El arreglo se inicializa con los nom-
 * bres de las tres funciones. Cuando el usuario introduce un valor entre 0 y 2, el valor se utiliza como el subíndice
 * del arreglo de apuntadores a funciones. La llamada a la función (línea 26) se hace de la siguiente manera:
 * (*f[ eleccion ])( eleccion );
 * 
 * @author Franklin Lugo
 * @version 1.0
*/

#include <stdio.h>

/*prototipos*/
void funcion1(int a);
void funcion2(int b);
void funcion3(int c);

int main()
{
	/*inicializa el arreglo de 3 apuntadores con funciones que toman un argumento entero y devuelven void*/

	void(*f[3])(int) = {funcion1, funcion2, funcion3 };

	int eleccion;

	printf("Introduzca un numero entre 0 y 2, 3 para terminar: ");
	scanf("%d", &eleccion);

	/*procesa la eleccion del usuario*/
	while( eleccion >= 0 && eleccion < 3)
	{
		/*invoca a la funcion en la ubicacion de la eleccion en el arreglo f
		y para la eleccion como argumento*/
		(*f[eleccion])(eleccion);

		printf("Introduzca un numero entre 0 y 2, 3 para terminar: ");
		scanf("%d", &eleccion);

	}//fin while

	printf("Termina la ejecucion del programa\n");

	return 0;
}//fin main

void funcion1(int a)
{
	printf("Usted introdujo %d de manera que invoco a la funcion1\n\n", a );
}//fin funcion1

void funcion2(int b)
{
	printf("Usted introdujo %d de manera que invoco a la funcion2\n\n", b );
}//fin funcion2

void funcion3(int c)
{
	printf("Usted introdujo %d de manera que invoco a la funcion3\n\n", c );
}//fin funcion3
