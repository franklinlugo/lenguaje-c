/*46_punteros.c
 *
 * impresion de una cadena caracter por caracter mediante
 * un apuntador  no constante a un dato constante
 *
 *@author Franklin Lugo
 *@version 1.0
 *
*/

#include <stdio.h>

void imprimeCaracteres(const char *ptrS); /*prototipo de la funcion*/

int main(){

	char cadena[] = "imprime los caracteres de una cadena";

	printf("La cadena es: \n");
	imprimeCaracteres(cadena);
	printf("\n");

	return 0;
}//fin main

/*ptrS no puede imprimir un caracter al cual no apunta
es decir, ptrS es un apuntador de solo lectura*/

void imprimeCaracteres(const char *ptrS){
	for(;*ptrS != '\0'; ptrS++){
		printf("%c", *ptrS);
	}//fin for
}//fin funcion
