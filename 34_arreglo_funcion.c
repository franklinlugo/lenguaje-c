/*34_arreglo_funcion.c
 *
 * Paso de arreglos y de elementos  de un arreglo a funciones
 *
 * @author Franklin Lugo
 * @version 1.0 
*/ 

#include <stdio.h>
#define TAMANIO 5

//Prototipos de las funciones
void modificaArreglo(int b[], int tamanio);
void modificaElemento(int e); 

int main(){

    int a[TAMANIO] = {0, 1, 2, 3, 4};   //inicializa a
    int i;                          //contador
    
    printf("efectos de pasar arreglos completos por referencia:\n\n"
    "Los valores del arreglo original son:\n");
    
    //muestra el arreglo original
    for( i = 0 ; i < TAMANIO ; i++ ){
        printf("%3d", a[i]);
    }//fin for

        printf("\n");
    
    //para el arreglo a modificaArreglo por referencia
    modificaArreglo( a, TAMANIO );
    
    printf("Los valores del arreglo modificado son:\n");
    
    //muestra el arreglo modificado
    for( i = 0 ; i < TAMANIO ; i++ ){
        printf("%3d", a[i]);
    }//fin for

        printf("\n");
    
    //muestra el valor de a[3]
    printf("\n\nEfectos de pasar un elemento del arreglo"
    "por valor:\n\nEl valor de a[3] es %d\n", a[3]);
    
    modificaElemento(a[3]); //pasa el elemento a[3]
    
    //muestra el valor a[3]
    printf("El valor de a[3] es:%d\n", a[3]);    

    return 0;
}//fin main


//en la función modificaArreglo, “b” apunta al arreglo original “a” en memoria 
void modificaArreglo(int b[], int tamanio){

    int j; //contador
    
    //multiplica cada elemento 
    for( j = 0 ; j < TAMANIO ; j++){
        b[j] *= 2;
    }//fin for

}//fin modificaArreglo

/*en la función modificaElemento, “e” es una copia local del elemento
 *a[3]del arreglo se pasa desde main */
void modificaElemento(int e){
    //multiplica el parametro por 2
    printf("El valor en modificaElemento es %d\n", e *= 2);
}//fin modificaElemento
