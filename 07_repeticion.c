/*
 * Programa que realiza el analisis de los resultados de un
 * examen                           -  07_repeticion.c
 * 
 * @author Franklin Lugo
 * @version 1.0
*/ 

#include <stdio.h>

int main () {
    
    int aprobados   = 0;    // cantidad de aprobados
    int reprobados  = 0;    // cantidad de reprobados
    int estudiante  = 1;    // contador de estudiantes
    int resultado;          // resultado de un examen
    
    // ciclo que procesara 10 estudiantes
    while (estudiante <= 10) {
        
        printf("Introduzca el resultado (1=Aprobado / 2=Reprobado): ");
        scanf("%d", &resultado);
        
        if (resultado == 1) aprobados++;
        else reprobados++;
        
        estudiante++;
    
    }//fin while
    
    printf("Aprobados %d\n", aprobados);
    printf("Reprobados %d\n", reprobados);
    
    if (aprobados >=8){
        printf("Objetivo alcanzado\n");
    }
    
    
    return 0;

}// fin main
