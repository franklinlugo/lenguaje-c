/*92_archivo.c

	crea un archivo secuencial
*/

#include <stdio.h>

int main()
{
	int cuenta; 	/*Numero de cuenta*/
	char nombre[30]; /*Nombre de la cuenta*/
	double saldo; 	/*Saldo de la cuenta*/

	FILE *ptrCF;	/* ptrCF = Apuntador al archivo clientes.dat*/

	/*fopen abre el archivo. Si no es capaz de crear el archivo sale del programa*/

	if((ptrCF = fopen( "cliente.dat", "w")) == NULL){
		printf("El archivo no pudo abrirse\n");
	}
	else{
		printf("Introduzca la cuenta el nombre y el saldo.\n");
		printf("Introduzca EOF al final de la entrada\n");
		printf("? ");
		scanf("%d%s%lf", &cuenta, nombre, &saldo);

		/*Escribe la cuenta, el nombre y el saldo dentro de un archivo con fprintf*/
		while( !feof( stdin )){
			fprintf( ptrCF, "%d%s%.2f\n", cuenta, nombre, saldo );
			printf("? ");
			scanf("%d%f%lf", &cuenta, nombre, &saldo);
		}//fin while

		fclose(ptrCF); /*fclose cierra el archivo*/
	}//fin else

	return 0;

}//fin main