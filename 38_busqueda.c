/*38_busqueda.c
 *  
 * Busqueda lineal en un arreglo 
 *
 * @author Franklin Lugo
 * @version 1.0 
*/ 

#include <stdio.h>
#define TAMANIO 100

 int BusquedaLineal(const int arreglo[], int llave, int tamanio);

 int main(){

    int a[TAMANIO];         //crea el arreglo a
    int x;                  //contador
    int llaveBusqueda;      //valor para localizar en el arreglo
    int elemento;           //Variable para almacenar la ubicacion de la llave busqueda

    /*Crea los datos*/
    for ( x = 0; x < TAMANIO; x++)
        {
            a[x] = 2 * x; 
        }

    printf("Introduzca la llave de busqueda entera\n");
    scanf("%d", &llaveBusqueda);

    /*Intenta localizar llaveBusqueda en el arreglo*/
    elemento = BusquedaLineal(a, llaveBusqueda, TAMANIO);

    /*Despliegue de resultados*/
    if(elemento != -1){
        printf("Encontre el valor en elemento %d\n", elemento );
    }//fin if
    else{
        printf("Valor no encontrado\n");
    }


    return 0;
 }//fin main

/*
compara la llave con cada elemento del arreglo hasta que localiza el
elemento o hasta que alcanza el final del arreglo; devuelve el subíndice del
elemento si lo encontró o -1 si no lo encontró
*/

 int BusquedaLineal(const int arreglo[], int llave, int tamanio){

    int n; //contador

    /*Ciclo del arreglo*/

    for( n = 0; n <= tamanio ; ++n){

        if (arreglo[n] == llave){
            return n; //devuelve ubicacion de la llave
        }//if
    }//for

    //en caso de no encontrar la llave
    return -1;


 }//fin BusquedaLineal
