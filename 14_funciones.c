/*
 * creacion y uso de una funcion definida por el usuario - 14_funciones.c
 * 
 * @author Franklin Lugo
 * @version 1.0
*/ 
#include <stdio.h>

int cuadrado (int y); // prototipo de la funcion

int main() {
    
    int x; //contador del for
    
    for (x = 1; x <= 10; x++) {
        
        printf("%d ", cuadrado ( x ) ); // llamada a la funcion
        
    }// fin for
    
    printf( "\n" );     

    return 0;

}//fin main

int cuadrado (int y){
    
    return y * y;
}
