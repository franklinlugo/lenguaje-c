/*
 * Programa que encuentra el maximo de 3 numeros - 15_funciones.c
 * 
 * @author Franklin Lugo
 * @version 1.0
 * 
*/ 

#include <stdio.h>

int maximo (int x, int y, int z); //Prototipo de una funcion

int main () {
    
    int numero1;
    int numero2;
    int numero3;
    
    printf("Introduzca tres numeros\n");
    scanf("%d%d%d", &numero1, &numero2, &numero3);
    
    //las variables son argumentos de la funcion
    printf("El maximo de los tres numeros es: %d\n", maximo (numero1, numero2, numero3));
    
    return 0;

}// fin main

int maximo (int x, int y, int z){

    int max = x; // se asume que x es el mayor
    
    if (y > max){
        max = y;
    }
    
    if (z > max){
        max = z;
    }
    
    return max;

}// fin funcion maximo

