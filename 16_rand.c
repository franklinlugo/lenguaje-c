/*16_rand.c
 * escalamiento y cambio de enteros producidos por 1 + rand() % 6 
 * 
*/ 

#include <stdlib.h>
#include <stdio.h>

int main () {

    int i;
    
    for (i = 1;i <= 20;i++) {

        // obtiene y despliega un numero entre 1 y 6 
        printf("%10d", 1 + (rand() % 6));
        
        // si el contador en divisible entre 5 comienza una linea de salida
        if (i % 5 == 0) {
            printf("\n");
        }// fin if
    }// fin for
    
        
    return 0;
}// fin main
