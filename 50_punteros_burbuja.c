/*50_punteros_burbuja.c
 *
 * Este programa coloca valores dentro de un arreglo,
 * ordena los valores en orden ascendente, e imprime los resultados de un arreglo
 *
 *@author Franklin Lugo
 *@version 1.0
 *
*/

#include <stdio.h>
#define TAMANIO 10

void ordenaMBurbuja(int *const arreglo, const int tamanio);

int main(){
	int a[TAMANIO] = {2,6,4,8,10,12,89,68,45,37};

	int i; /*contador*/ 

	printf("Elementos en el orden original\n");
	for( i = 0 ; i < TAMANIO ; i++ ){
		printf( "%4d", a[i] );
	}//fin for

	ordenaMBurbuja(a, TAMANIO); /*funcion que ordena el arreglo*/

	printf("\n");

	printf("Elementos del arreglo en orden ascendente\n");
	for( i = 0 ; i < TAMANIO ; i++ ){
		printf( "%4d", a[i] );
	}//fin for	

	printf("\n");

	return 0;

}//fin main

void ordenaMBurbuja(int *const arreglo, const int tamanio){
	void intercambia(int *prtElemento1, int *prtElemento2); /*prototipo*/
	int pasada;
	int j;

	for( pasada = 0 ; pasada < tamanio - 1; pasada++)
	{
		for( j = 0 ; j < tamanio - 1 ; j++)
		{
			if(arreglo[j]>arreglo[j+1])
			{
				intercambia( &arreglo[j], &arreglo[j+1]);

			}//if

		}//for interno 
	}
}//fin funcion

void intercambia(int *prtElemento1, int *prtElemento2){
	int almacena  = *prtElemento1;
	*prtElemento1 = *prtElemento2;
	*prtElemento2 = almacena;
}//fin funcion
