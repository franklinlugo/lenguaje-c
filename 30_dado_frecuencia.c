/*30_dado_frecuencia.c
 *
 * Lanza un dado de 6 lados 6000 veces
 * 
 * @author Franklin Lugo
 * @version 1.0 
*/ 

#include <stdio.h> 
#include <stdlib.h> 
#include <time.h>
#define TAMANIO 7

int main(){
    
    int cara;                           //valor aleatorio entre 1 y 6 
    int tiro;                           //contador de tiros del 1 al 6000
    int frecuencia[TAMANIO] = {0};      //inicializa a 0 la cuenta
    srand(time(NULL));                  // generador de la semilla de numeros alearios
    
    //tira el dado 6000 veces
    for( tiro = 1 ; tiro <= 6000 ; tiro++ ){
        
        cara = 1 + rand() %6;
        ++frecuencia[cara];
        
    }//fin for

    printf("%s%17s\n", "Cara", "Frecuencia");
    
    for( cara = 1 ; cara < TAMANIO ; cara++){
        printf("%4d%17d\n", cara, frecuencia[cara]);
    }//fin for

    return 0;
}//fin main 

