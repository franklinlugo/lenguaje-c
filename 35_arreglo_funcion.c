/*35_arreglo_funcion.c
 *
 * Demostracion del calificador  de tipo const con arreglos
 *
 * @author Franklin Lugo
 * @version 1.0 
*/ 

#include <stdio.h>

void intentaModifArreglo(const int b[]);   //prototipo de la funcion

int main(){

    int a[] = { 10, 20, 30 };
    
    intentaModifArreglo(a);
    
    printf(" %d %d %d \n" a[0], a[1], a[2]);

    return 0;
}//fin main

/* en la función intentaModifElArreglo, el arreglo b es const, por lo tanto
 * no puede ser
 * utilizado para modificar el arreglo original a en main. */
void intentaModifArreglo(const int b[]){

    b[0] /= 2;   //error
    b[1] /= 2;   //error
    b[2] /= 2;   //error
    
}// fin funcion intentaModifArreglo
 
