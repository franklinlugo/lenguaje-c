/*20_alcance.c
 * Ejemplo de alcance
 * 
*/ 

#include <stdio.h>

void usoLocal(void); // prototipo de funcion
void usoStaticLocal(void); // prototipo de funcion
void usoGlobal(void); // prototipo de funcion

int x = 1; // variable global 

int main() {
    
    int x = 5; // variable local en main
    printf("la X local fuera del alcance de main es: %d\n", x);
    
    { // nuevo alcance
        int x = 7; // var local con nuevo alcance
        printf("la X local en el alcance interno es: %d\n", x);
    } // fin nuevo alcance
    
    printf("La X local en el alcance interno de main es: %d\n", x);
    
    usoLocal();         // usoLocal contiene una x local
    usoStaticLocal();   // usoStaticLocal contiene una x local estatica
    usoGlobal();        // usoGlobal utiliza un x global
    usoLocal();         // usoLocal reinicializa la x local automatica
    usoStaticLocal();   // static local  x retiene su valor previo
    usoGlobal();        // x global tambien retiene su valor
    
    printf("\nLa x local en main es %d\n", x);
    
    return 0;
    
}// fin main

// usoLocal reinicializa a la variable local x durante cada llamada

void usoLocal(void){
    
    int x = 25; // se inicializa cada vez que se llama a uso local
    printf("\nLa x local en usoLocal es %d despues de entrar en usoLocal\n", x);
    x++;
    printf("La x local en usoLocal es %d antes de salir de usoLocal\n", x);

}// fin funcion usoLocal

/*
 * usoStaticLocal inicializa la variable static Local x solo la primera
 * vez que se invoca a la funcion; el valor de x se guarda entre las llamadas
 * a esta funcion*/

void usoStaticLocal(){

    // se inicializa la primera vez que se invoca a usoStaticLocal
    static int x = 50;
    
    printf("\nla x local estatica  es %d al entrar a usoStaticLocal\n", x);
    x++;
    printf("la x local estatica  es %d al salir de usoStaticLocal\n", x);
    
} // fin funcion usoStaticLocal

/*la funcion usoGlobal modifica la variable global x durante cada llamada*/

void usoGlobal(){

    printf("\nla x global es %d al entrar a usoGlobal\n", x);
    x *= 10;
    printf("la x global es %d al salir de usoGlobal\n", x);

}// fin funcion usoGlobal
