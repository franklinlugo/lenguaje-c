/*88_struct.c

	uso de los operadores de estructura miembro y de apuntador a estructura
*/

#include <stdio.h>

/*definicion de la estructura carta*/

struct carta{
	char *cara;
	char *palo;
}; //fin struct carta

int main()
{
	struct carta unaCarta; /*define una estructura variable carta*/
	struct carta *ptrCarta; /*define un apuntador a  una estructura carta*/

	/*coloca cadenas dentro de una carta*/

	unaCarta.cara = "As";
	unaCarta.palo = "Espadas";

	ptrCarta = &unaCarta; /*asigna la direccion de unaCarta a ptrCarta*/

	printf("%s%s%s\n%s%s%s\n%s%s%s\n", unaCarta.cara, " de ", unaCarta.palo,
	ptrCarta->cara, " de ", ptrCarta->palo,
	( *ptrCarta ).cara, " de ", ( *ptrCarta ).palo );
	return 0;
}//fin main
