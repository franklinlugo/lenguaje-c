/*91_enum.c

 Uso de un tipo de enumeracion

*/

#include <stdio.h>

/* Las constantes de enumeracion representan los meses del anio*/
enum meses{
	ENE = 1, FEB, MAR, ABR, MAY, JUN, JUL, AGO, SEP, OCT, NOV, DIC };

int main()
{
	enum meses mes; /* puede contener cualquiera de los doce meses*/

	/* inicializa el arreglo de apuntadores */
	const char *nombreMes[] = { "", "Enero", "Febrero", "Marzo",
		"Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre",
		"Noviembre", "Diciembre" };

	/* ciclo a través de los meses */
	for ( mes = ENE; mes <= DIC; mes++ ) 
	{
		printf( "%2d%11s\n", mes, nombreMes[ mes ] );
	} /* fin de for */

	return 0;

}//fin main