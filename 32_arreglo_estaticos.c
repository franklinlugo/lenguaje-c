/*32_arreglo_estaticos.c
 * 
 *
 * @author Franklin Lugo
 * @version 1.0 
*/ 

#include <stdio.h>

void iniciaArregloEstatico(void);     //prototipo de la funcion
void iniciaArregloAutomatico(void);   //prototipo de la funcion
int main() {

    printf("Primera llamada a cada funcion: \n");
    iniciaArregloEstatico(); 
    iniciaArregloAutomatico();

    printf("\n\nSegunda llamada a cada funcion: \n");
    iniciaArregloEstatico(); 
    iniciaArregloAutomatico();

    return 0;
}//fin main 

//funcion para demostrar un arreglo estatico local 
void iniciaArregloEstatico(void){

    //inicializa los elementos a cero la primera vez que se llama a la funcion
    static int arreglo1[3];
    int i;
    
    printf("\nValores al entrar al inicioArregloEstatico\n");
    
    //muestra el contenido de arreglo1
    for( i = 0 ; i <= 2 ; i++ ){
        printf("arreglo1[%d] = %d ", i, arreglo1[i]);
    }//fin for

    printf("\nValores al salir de iniciaArregloEstatico\n");
    
    //modifica y muestra el contenido de arreglo1
    for( i = 0 ; i <= 2 ; i++ ){
        printf("arreglo1[%d] = %d ", i, arreglo1[i] += 5);
    }//fin for
    
}//fin funcion iniciaArregloEstatico

//funcion para demostrar un arreglo local automatico
void iniciaArregloAutomatico(void){

    int arreglo2[3] = {1, 2, 3};    //inicia los elementos cada vez que se llama a la funcion
    int i;                          //contador
    
    printf("\n\nValores al salir de iniciaArregloAutomatico:\n");
    for( i = 0 ; i <= 2 ; i++ ){
        printf("arreglo2[%d] = %d ", i, arreglo2[i] += 5);
    }//fin for

}//fin funcion iniciaArregloAutomatico
