/*40_arreglo.c
 *
 * Inicializacion de arreglos multidimensionales    
 *
 * @author Franklin Lugo
 * @version 1.0
*/

#include <stdio.h>
// prototipo de la funcion
void despliegaArreglo(const int a[][3]);

int main(){
    int arreglo1[2][3] = {{1,2,3},{4,5,6}};
    int arreglo2[2][3] = {{1,2,3,4,5}};
    int arreglo3[2][3] = {{1,2},{4}};

    printf("Los valores en el arreglo 1 son: \n");
    despliegaArreglo(arreglo1);
    
    printf("Los valores en el arreglo 2 son: \n");
    despliegaArreglo(arreglo2);
    
    printf("Los valores en el arreglo 3 son: \n");
    despliegaArreglo(arreglo3);

    return 0;

}//fin main

// funcion que muestra un arreglo con 2 filas y 3 columnas
void despliegaArreglo(const int a[2][3]){
    int i, j;

    //ciclo de las filas
    for( i = 0 ; i <= 1 ; i++){
        // columnas
        for( j = 0 ; j <= 2 ; j++){
            printf("%d ",a[i][j]);
        }// fin for columnas
        //comienza la nueva linea de salida
        printf("\n");
    }// fin for filas
}//fin funcion