/*
 * uso de break dentro de un for  - 12_break.c
 * 
 * @author Franklin Lugo
 * @version 1.0
*/ 

int main () {
    
    #include <stdio.h>

    int x; // contador
    
    // ciclo que se repetira 10 veces   
    for (x = 1; x <= 10; x++) {
        
        if (x == 5) break;
        
        printf("%d ", x);
    }// fin for 
    
    printf("\nRompe el ciclo en x == %d\n", x);
    return 0;
}// fin main
