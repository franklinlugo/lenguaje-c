/*39_busqueda.c
 *  
 * Busqueda binaria en un arreglo 
 *
 * @author Franklin Lugo
 * @version 1.0 
*/ 

 #include <stdio.h>
 #define TAMANIO 15

 /*Prototipos de las funciones*/
 int busquedaBinaria( const int b[], int claveBusqueda, int bajo, int alto);
 int despliegaEncabezado( void );
 int despliegaLinea( const int b[], int bajo, int medio, int alto);


 int main(){

    int a[TAMANIO];     //crea el arreglo
    int i;              //contador
    int llave;          //valor a localizar en el arreglo
    int resultado;      //para almacenar la ubicacion de la llave o -1

    for( i = 0 ; i <= TAMANIO ; i++ ){
        a[i] = 2 * i;
    }//fin for

    printf("Introduzca un numero entre 0 y 28: ");
    scanf("%d", &llave);

    despliegaEncabezado();

    //busca la llave en el arreglo a
    resultado = busquedaBinaria( a, llave, 0, TAMANIO - 1); 

    //despliegue de resultados
    if (resultado != -1){
        printf("\n%d se encuentra en el elemento %d del arreglo\n", llave, resultado);
    }//fin if
    else{
        printf("%d no se encuentra \n", llave );
    }

    return 0;
 }//fin main

// ######################
// ####funciones#########
// ######################


/*Funcion que realiza la busqueda binaria en un arreglo*/
int busquedaBinaria( const int b[], int claveBusqueda, int bajo, int alto){
    int central;    //variable para mantener  el elemento central del arreglo

    /*Realiza el ciclo  hasta que el subindice bajo es mayor que el subindice alto*/
    while( bajo <= alto){
        central = (bajo + alto ) / 2; //determina elemento central
        despliegaLinea( b, bajo, central, alto); //despliegue del arreglo utilizado en este ciclo

        /* si claveDeBusqueda coincide con el elemento central, devuelve central */
        if( claveBusqueda == b[central]){
            return central;
        }//fin if
            
        /* si claveBusqueda es menor que el elemento central, establece el 
        nuevo valor de alto */
        else if(claveBusqueda < b[central]){
            alto = central - 1; //busca en la mitad inferior del arreglo
        }//fin else if

        /* si claveDeBusqueda es mayor que el elemento central, establece el
        nuevo valor para bajo */
        else{
            bajo = central + 1; //busca en la mita superior del arreglo
        }//fin else
    }//fin while
    return -1; //no se encontro clave busqueda
}//fin funcion busquedaBinaria

/*imprime un encabezado para la salida*/
int despliegaEncabezado( void ){
    int i;

    printf("\nSubindices\n");

    //muestra el encabezado de la columna
    for( i = 0 ; i < TAMANIO ; i++){
        printf("%3d ", i );
    }

    printf("\n");

    //muestra una linea de caracteres
    for( i = 0 ; i <= 4 * TAMANIO ; i++){
        printf("-");
    }
    printf("\n");

}//fin funcion despliegaEncabezado

/*imprime una linea de salida que muestra la parte actual del arreglo que se esta
procesando*/
int despliegaLinea( const int b[], int bajo, int medio, int alto){
    int i;

    for( i = 0 ; i < TAMANIO ; i++){
        //despliega espacios si se encuentra  fuera del rango actual del subarreglo
        if( i < bajo || i > alto) printf("   ");
        else if( i == medio) printf("%3d*", b[i] );
        else printf("%3d", b[i] );
    }//fin for

    printf("\n");

}//fin despliegaLinea