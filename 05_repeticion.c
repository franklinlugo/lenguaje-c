/*
 * Programa para obtener promedio de calificaciones de un grupo
 * mediante repeticion controlada por contador - 05_repeticion.c
 * 
 * @author Franklin Lugo
 * @version 1.0
*/ 

#include <stdio.h>

int main () {

    int contador;       // numero de calificacion siguiente
    int calificacion;   // valor de la calificacion
    int total;          // suma de las calificaciones
    int promedio;       // promedio de las calificaciones   
    
    // inicializacion de variables
    total = 0;
    contador = 1;
    
    // procesamiento
    while (contador <= 10) { // este while permitira repetir 10 veces
        printf("Introduzca la calificacion del alumno: ");
        scanf("%d", &calificacion);
        total       = total + calificacion;
        contador    = contador + 1;
    } // fin del while
    
    // calculos de la fase de terminacion
    promedio = total / 10;
    
    printf("El promedio del grupo es: %d", promedio);
    
    return 0;

} // fin de la funcion main
