/*48_punteros.c
 *
 * Intenta modificar un apuntador constante a un dato no constante
 *
 *@author Franklin Lugo
 *@version 1.0
 *
*/

#include <stdio.h>

int main(){
	int x;
	int y;

	/* ptr es un apuntador constante a un entero que se puede modificar
	a través de ptr, pero ptr siempre apunta a la misma ubicación
	de memoria */

	int *const ptr = &x;

	*ptr = 7; /*permitido: *ptr no es const*/
	ptr = &y; /*error: ptr es const; no se puede asignar una nueva direccion*/

	return 0;
}//fin main