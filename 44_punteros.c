/*44_punteros.c
 *
 * Eleva al cubo una variable mediante una llamada por referencia, con un
 * apuntador como argumento
 *
 *@author Franklin Lugo
 *@version 1.0
 *
*/

#include <stdio.h>

void cuboPorReferencia(int *ptrN); //prototipo

int main(){

	int numero = 5;
	printf("El valor original de numero es %d\n", numero );

	cuboPorReferencia(&numero);

	printf("El nuevo valor de numero es %d\n", numero );

	return 0;

}//fin main

/*calcula el cubo de *ptrN; modifica la variable numero en main*/
void cuboPorReferencia(int *ptrN){

	*ptrN = *ptrN * *ptrN * *ptrN;

}//fin funcion
