/*49_punteros.c
 *
 * Intenta modificar un apuntador constante a un dato constante
 *
 *@author Franklin Lugo
 *@version 1.0
 *
*/

#include <stdio.h>

int main(){

	int x;
	int y;

	const int *const prt = &x;

	printf("%d\n", *ptr);

	*ptr = 7; /*error: *ptr es const; no se puede asignar un nuevo valor*/
	ptr = &y; /*error: ptr es const; no se puede asignar una nueva direccion*/

	return 0;
}//fin main