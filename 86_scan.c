/*86_scan.c

	entrada de datos con un ancho de campo

*/

#include <stdio.h>

int main()
{
	int x;
	int y;

	printf("Introduce un entero de 6 digitos:");
	scanf("%2d%d", &x, &y);

	printf("Los caracteres introducidos son %d y %d\n", x, y );
	return 0;
}//fin main	