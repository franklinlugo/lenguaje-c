/*64_strtod.c

uso de strtod

La función strtod convierte una secuencia de caracteres que representan un valor de punto
flotante a double. La función recibe dos argumentos, una cadena (char *) y un apuntador a una cadena
(char **). La cadena contiene la secuencia de caracteres que se convertirán a double. Al apuntador se le
asigna la ubicación del primer carácter después de la parte convertida de la cadena. La línea 

d = strtod( cadena, &ptrCadena );

indica que a d se le asigna el valor double convertido de la cadena, y a ptrCadena se le asigna la ubicación
del primer carácter después del valor convertido en cadena.
	
@author Franklin Lugo
@version 1.0
*/

#include <stdio.h>
#include <stdlib.h>

int main(){

	/*inicializa el apuntador cadena*/
	const char *cadena = "51.2% son admitidos"; /*inicializa la cadena*/
	double d;
	char ptrCadena; /*crea un apuntador char*/

	d = strtod(cadena, &ptrCadena);

	printf("la cadena \"%s\" se convierte en \n", cadena );
	printf("un valor double %.2f y la cadena\"%s\"\n", d, cadena );

	return 0;
}//fin main
