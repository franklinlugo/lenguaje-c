/*61_atof.c

Uso de atof

La función atof convierte su argumento, una cadena que representa un número de
punto flotante, a un valor double. La función devuelve el valor double. Si el
valor convertido no puede representarse, por ejemplo, si el primer carácter de 
la cadena no es un dígito, el comportamiento de la función atof es indefinido.

Cuando utilice funciones de la biblioteca general de utilidades, 
incluya el encabezado <stdlib.h>

@author Franklin Lugo
@version 1.0
*/

#include <stdio.h>
#include <stdlib.h>

int main()
{
	double d;
	d = atof("99.0");

	printf( "%s%.3f\n%s%.3f\n",
	"La cadena \"99.0\" convertida a double es ", d,
	"El valor convertido divivido entre 2 es ",
	d / 2.0 );

	return 0;
}//fin main