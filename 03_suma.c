/*
 * Programa que suma dos numeros - 03_suma.c
 * 
 * @author  Franklin Lugo
 * @version 1.0
*/ 

#include <stdio.h>

int main (void) {

    int num1;
    int num2;
    int suma;

    printf("ingrese el primer numero: ");
    scanf("%d", &num1);                     // Lee un entero - 
    printf("ingrese el segundo numero: ");
    scanf("%d", &num2);                         // funcion scanf de la biblioteca estandar
    
    
    suma = num1 + num2;
    // el operador %d indica que se imprimira un entero
    printf("El resultado de la suma de los numeros es: %d\n", suma);

    return 0;
}

/*
 * el especificador de conversion %d indica que el dato debe ser un entero
 * la letra d significa entero decimal.
 * el ampersan (&) se conoce como operador de direccion
*/ 
