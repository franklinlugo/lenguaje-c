/*
 * Uso de if, operadores de relacion e igualdad - 04_operadores.c
 * 
 * @author  Franklin Lugo
 * @version 1.0
*/ 

#include <stdio.h>

// funcion main que inicia la ejecucion del programa    
int main () {
    
    int num1;
    int num2;
    
    printf("Introduzca dos valores enteros\n");
    printf("y le dire las relaciones que satisfacen: ");
    
    scanf("%d%d", &num1, &num2); // lectura de los enteros
    
    if (num1 == num2) {
        
        printf("%d es igual que %d\n", num1, num2);
        
    }

    if (num1 != num2) {
        
        printf("%d  no es igual que %d\n", num1, num2);
        
    }
    
    if (num1 < num2) {
        
        printf("%d  es menor que %d\n", num1, num2);
        
    }
    
    if (num1 > num2) {
        
        printf("%d  es mayor que %d\n", num1, num2);
        
    }
    
    if (num1 <= num2) {
        
        printf("%d  es menor o igual que %d\n", num1, num2);
        
    }
    
    if (num1 >= num2) {
        
        printf("%d  es mayor o igual que %d\n", num1, num2);
        
    }
    
    
    
    return 0; // indica que el programa termino con exito
} // fin del main
