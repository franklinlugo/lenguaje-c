/*25_arreglos.c
 *
 * Especificar tamaño del arreglo mediante constante simbolica 
 *  
 * @author Franklin Lugo
 * Version 1.0
*/ 
 
#include <stdio.h>
#define TAMANIO 10

int main () {
    int s[TAMANIO];
    int j;          //contador
    
    for(j = 0; j < TAMANIO; j++) {
        s[j] = 2 + 2 * j; 
    }//fin for
    
    printf("%s%13s\n","elemento","valor");
    
    //muestra el contenido del arreglo s en forma tabular
    for(j = 0; j < TAMANIO; j++) {
        printf("%7d%13d\n", j, s[j]);
    }//fin for
    return 0;
}//fin main
