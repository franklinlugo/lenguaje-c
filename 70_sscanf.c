/*70_sscanf.c

uso de sscanf

*/

#include <stdio.h>

int main()
{
	char s[] = "31298 87.375";
	int x;
	double y;

	sscanf( s, "%d%lf", &x, &y );

	printf("%s\n%s%6d\n%s%8.3f\n"
		"Los valores almacenados en el arreglo de caracteres s son:",
		"entero:", x, "double:", y); 
	return 0;
}