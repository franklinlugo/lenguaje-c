/*26_arreglos.c
 *
 *Sumar los elementos de un arreglo  
 * @author Franklin Lugo
 * Version 1.0
*/ 
 
#include <stdio.h>
#define TAMANIO 12

int main () {
    int a[TAMANIO] = {1, 3, 5, 4, 7, 2, 99, 16, 45, 67, 89, 45};
    int i;          //contador
    int total = 0;      //suma del arreglo
    
    //suma del contenido del arreglo
    for( i = 0 ; i < TAMANIO ; i++) {
        total += a[i];
    }//fin for
    
    printf("El total de los elementos del arreglo es %d\n", total);
    
    return 0;
}//fin main
