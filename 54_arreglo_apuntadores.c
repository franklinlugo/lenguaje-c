/* 54_arreglo_apuntadores.c
 *
 * copia de una cadena por medio de la notacion de arreglos y 
 * la notacion de apuntadores
 *
 *@author Franklin Lugo
 *@version 1.0
 */

#include <stdio.h>

void copia1( char *s1, const char *s2);
void copia2( char *s1, const char *s2);

int main(){
	char cadena1[10];
	char *cadena2 = "hola"; /*apuntador a una cadena*/
	char cadena3[10];
	char cadena4[] = "adios"; /*apuntador a una cadena*/

	copia1(cadena1, cadena2);
	printf("Cadena1 = %s\n", cadena1 );

	copia2(cadena3, cadena4);
	printf("Cadena3 = %s\n", cadena3 );

}//fin main

/*copia s2 en s1 con el uso de la notacion de arreglos*/
void copia1( char *s1, const char *s2){
	int i;

	for( i = 0 ; ( s1[i] = s2[i] ) != '\0' ; i++) {
		; /*no realiza tarea alguna en el cuerpo*/
	}//
}//fin copia1

/*copia s2 en s1 con el uso de la notacion de apuntadores*/
void copia2( char *s1, const char *s2){
	for( ; ( *s1 = *s2) != '\0' ; s1++, s2++ ){
		;
	}//
}//fin copia2