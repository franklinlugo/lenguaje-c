/*69_sprintf.c

uso de sprintf

*/

#include <stdio.h>

int main(){
	char s[80]; /*arreglo de caracteres*/
	int x;
	double y;

	printf("Introduzca un entero y un double: \n");
	scanf("%d%lf", &x, &y);

	sprintf(s, "entero: %6d\ndouble:%8.2f", x, y);

	printf("%s\n%s\n"
		"La salida con formato, almacenada en el arreglo s, es:", s);

	return 0;

}//fin main