/*75_printfcc.c

impresion de cadenas y caracteres

*/

#include <stdio.h>

int main()
{

	char caracter = 'A';
	char cadena[]= "Esta es una cadena";
	const char *ptrCadena = "Esta tambien es una cadena";

	printf( "%c\n", caracter);
	printf( "%s\n", "esta es una cadena" ); /*las cadenas de caracteres deben encerrarse entre comillas dobles*/
	printf( "%s\n", cadena );
	printf( "%s\n", ptrCadena );

	return 0;
}//fin main