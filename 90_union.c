/*90_union.c

	Ejemplo de union

*/

#include <stdio.h>

/* definicion de la union numero */
union numero{
	int x;
	double y;
};

int main()
{
	union numero valor;	/* define la variable de union */
	valor.x = 100; /* coloca un entero dentro de la union */

	printf( "%s\n%s\n%s%d\n%s%f\n\n",
		"Coloca un valor en el miembro entero",
		"e imprime ambos miembros.",
		"int:	", valor.x,
		"double:\n", valor.y );

	valor.y = 100; /* coloca un entero dentro de la misma union */
	printf( "%s\n%s\n%s%d\n%s%f\n",
		"Coloca un valor en el miembro flotante",
		"e imprime ambos miembros.",
		"int:	", valor.x,
		"double:\n", valor.y );


	return 0;

}//fin main