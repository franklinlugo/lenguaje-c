/*23_arreglos.c
 * 
 *Declarar un arreglo y utilizar un ciclo para iniciar sus elementos
 *  
 * @author Franklin Lugo
 * Version 1.0
*/ 
 
#include <stdio.h>

int main () {
    int n[10];  //n es un arreglo de 10 enteros
    int i;      //contador
    
    //para inicializar los elementos del arreglo n a 0
    for (i = 0; i < 10 ; i++) {
            n[i] = 0; 
    }//fin for    
    
    printf("%s%13s\n", "elemento", "valor");
    for (i = 0; i < 10; i++){
        printf("%7d%13d\n", i, n[i]);
    }//fin for

    return 0;
}//fin main
