/*18_tiro_dado_randomizado.c
 * Randomizacion del programa de dados
 * 
*/ 

#include <stdio.h>
#include <stdlib.h>

int main () {
	
    int i; 
    unsigned semilla; // numero que se utilizara para establecer semilla

    
    printf("Introduzca la semilla: ");
    scanf("%u",  &semilla ); // %u para unsigned
    
    srand(semilla);
    
    for(i = 1 ; i <= 10  ; i++) {

        printf( "%10d", 1 + (rand() % 6));
        
        if (i % 5 == 0) {

            printf("\n");

        } //fin if

    } // fin for
    
    return	0; 

} // fin main 
