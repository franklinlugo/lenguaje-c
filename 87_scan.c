/*87_scan.c

	Lectura y descarte de caracteres desde el flujo de entrada
*/

#include <stdio.h>

int main()
{
	int mes1;
	int dia1;
	int anio1;
	int mes2;
	int dia2;
	int anio2;

	printf("Introduzca una fecha en el formato mm-dd-aaaa:");
	scanf("%d%*c%d%*c%d", &mes1, &dia1, &anio1 );

	printf("mes = %d dia = %d anio = %d\n\n", mes1, dia1, anio1 );

	printf("Introduzca una fecha en el formato mm-dd-aaaa:");
	scanf("%d%*c%d%*c%d", &mes2, &dia2, &anio2 );

	printf("mes = %d dia = %d anio = %d\n\n", mes2, dia2, anio2 );
	return 0;
}//fin main