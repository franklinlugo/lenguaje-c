/*24_arreglos.c
 * 
 *Declarar un arreglo e iniciarlo mediante una lista de inicializacion
 *  
 * @author Franklin Lugo
 * Version 1.0
*/ 
 
#include <stdio.h>

int main () {
    int n[10] = {32, 27, 24, 18, 95, 14, 90, 70, 60, 37};  
    int i;      //contador
    
    printf("%s%13s\n", "elemento", "valor");
    for (i = 0; i < 10; i++){
        printf("%7d%13d\n", i, n[i]);
    }//fin for

    return 0;
}//fin main
