/*67_getsputchar.c

uso de gets y putchar

@author Franklin Lugo
@version 1.0
*/

#include <stdio.h>

int main()
{
	char enunciado[80];/*crea un arreglo de caracteres*/
	printf("Introduzca una linea de texto\n");

	/*utiliza gets para leer una linea de texto*/
	gets(enunciado);

	printf("La linea impresa al revez es: \n");
	inverso(enunciado);

	return 0;
}//fin main

/*imprime recursivamente los caracteres de una cadena en orden inverso*/
void inverso( const char * const ptrS)
{
	/*si es el final de la cadena*/
	if(ptrS[0] == '\0') /*caso base*/
	{
		return;
	}
	else /*si no es el final de la cadena*/
	{
		inverso( &ptrS[1]); /*paso recursivo*/
		putchar( ptrS[0]); /*utiliza putchar para desplegar los caracteres*/
	}

}//fin inverso