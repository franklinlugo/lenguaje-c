/*85_scan.c

	uso de un conjunto de exploracion invertido

*/

#include <stdio.h>

int main()
{
	char z[9];

	printf("Introduzca una cadena: ");
	scanf( "%[^aeiou]", z); /*conjunto de exploracion invertido*/

	printf("La entrada es \"%s\" \n", z );
	return 0;
}//fin main

