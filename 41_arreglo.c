/*41_arreglo.c
 *
 * Ejemplo de un arreglo de doble subindice
 *
 * @author Franklin Lugo
 * @version 1.0
*/

#include <stdio.h>
#define ESTUDIANTES 3
#define EXAMENES 4

// prototipos de las funciones
int minimo(const int calificaciones[][EXAMENES], int alumnos, int examenes);
int maximo(const int calificaciones[][EXAMENES], int alumnos, int examenes);
double promedio(const int estableceCalif[], int examenes);
void despliegaArreglo(const int calificaciones[][EXAMENES], int alumnos, int examenes);


int main()
{
	int estudiante;

	//inicializa las calificaciones para tres estudiantes
	const int calificacionesEstudiantes[ESTUDIANTES][EXAMENES]=
	{
		{77,68,86,73},
		{96,87,89,78},
		{70,90,86,81}
	};

	//muestra el arreglo CalificacionesEstudiantes
	printf("El arreglo es:\n");
	despliegaArreglo(calificacionesEstudiantes, ESTUDIANTES, EXAMENES);

	//calificacion mas alta y mas baja
	printf("\n\nCalificacion mas baja: %d\nCalificación mas alta: %d\n",
		minimo(calificacionesEstudiantes, ESTUDIANTES, EXAMENES),
		maximo(calificacionesEstudiantes, ESTUDIANTES, EXAMENES));

	//promedio de calificacion de cada estudiante
	for (estudiante = 0; estudiante < ESTUDIANTES; estudiante++ )
	{
		printf( "El promedio de calificacion del estudiante %d es %.2f\n",
			estudiante, promedio(calificacionesEstudiantes[estudiante], EXAMENES));
	}

	return 0;
}//fin main

void despliegaArreglo(const int calificaciones[][EXAMENES], int alumnos, int examenes)
{
	int i; //contador de estudiantes
	int j; //contador de examenes
	
	// muestra el encabezado de las columnas
	printf( "                             [0]  [1]  [2]  [3]");

	// muestra las calificaciones en forma tabular 
	for ( i = 0; i < alumnos; i++ )
	{
		// muestra la etiqueta de la fila 
		printf( "\ncalificacionesEstudiantes[%d] ", i );
			
		// muestra las calificaciones de un estudiante 
		for ( j = 0; j < examenes; j++ )
		{
			printf( "%-5d", calificaciones[ i ][ j ]);
		}//fin for interno
	}//fin for externo
	
	printf( "\n");

}//fin función despliegaArreglo

//Encuentra la calificación mínima
int minimo(const int calificaciones[][EXAMENES], int alumnos, int examenes)
{
	int i; //contador de estudiantes
	int j; //contador de examenes
	int califBaja = 100; //inicializa a la calificación más alta posible
	
	//ciclo a través de las filas de calificaciones
	for ( i = 0; i < alumnos; i++ )
	{
		/* ciclo a través de las columnas de calificaciones */
		for ( j = 0; j < examenes; j++ )
		{
			if ( calificaciones[ i ][ j ] < califBaja )
			{
				califBaja = calificaciones[ i ][ j ];
			} //fin if
		} //fin for interno
	}//fin for externo
	
	return califBaja; /* devuelve la calificación mínima */
}// fin función minimo

//encuentra la calificacion maxima
int maximo(const int calificaciones[][EXAMENES], int alumnos, int examenes)
{
	int i; //contador estudiantes
	int j; //contador examenes
	int califAlta = 0; //inicializa la calificacion mas baja posible

	//ciclo a través de las filas de calificaciones
	for ( i = 0; i < alumnos; i++ )
	{
		/* ciclo a través de las columnas de calificaciones */
		for ( j = 0; j < examenes; j++ )
		{
			if ( calificaciones[ i ][ j ] > califAlta)
			{
				califAlta = calificaciones[ i ][ j ];
			} //fin if
		} //fin for interno
	}//fin for externo
	return califAlta;
}//fin funcion maximo
//calcula el promedio de notas por alumno
double promedio( const int conjuntoDeCalificaciones[], int examenes )
{
	int i; // contador de exámenes
	int total = 0; // suma de las calificaciones del examen

	// total de calificaciones de un estudiante
	for ( i = 0; i < examenes; i++ )
	{
		total += conjuntoDeCalificaciones[ i ];
	}
	return ( double ) total / examenes; //promedio
}//fin de la función promedio

/*
 *	un arreglo con dos subíndices es básicamente un arreglo
 *	formado por arreglos con un solo subíndice, y que el 
 *	nombre de un arreglo con un solo subíndice es la 
 *	dirección en memoria de ese arreglo. 
*/