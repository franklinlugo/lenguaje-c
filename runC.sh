#!/bin/bash
# Author: Franklin Lugo
# Description: script para compilar programas en lenguaje C desde la consola de linux 
# utilizando gcc
# ejemplo: ./runC.sh programa.c programa

# gcc es para compilar codigo en C y g++ para compilar codigo en C++


gcc -Wall $1 -o $2 && ./$2